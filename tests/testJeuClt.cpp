#include "../src/level.hpp"
#include "../src/ui/graphicsCurses.hpp"
#include "../src/network/client.hpp"

int main(int argc, char** argv) {
    int x, y, player, type;
    Level* game = new Level(40);
    Graphics* UI = new Graphics(game);
    if (argc < 3)
	exit(1);
    Client* C = new Client(argv[1], argv[2]);
    C->connectSrv();

    UI->initMap();
    UI->refreshMap();
    C->morai();
    C->getMove(&x, &y, &player, &type);
    printf("%d %d %d %d", x, y, player, type);
    game->place(x, y, player, type);
    UI->refreshMap();

    while (UI->getAction(&x, &y, &type) != 'q') {
	game->place(x, y, 2, type);
	UI->refreshMap();
	C->okuru(x, y, 2, type);
	C->morai();
	C->getMove(&x, &y, &player, &type);
	printf("%d %d %d %d", x, y, player, type);
	game->place(x, y, player, type);
	UI->refreshMap();
    }

    return 0;
}
