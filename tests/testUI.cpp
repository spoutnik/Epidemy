#include "../src/ui/graphicsCurses.hpp"

int main() {
    Level* game = new Level(40);
    Graphics* UI = new Graphics(game);

    if (UI->initMap()) {
	UI->refreshMap();

	UI->interpret();
    }

    delete UI;

    game->dumpMoves(stdout);
    delete game;
}
