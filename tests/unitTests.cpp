#include "../src/cellStack.hpp"

#include <time.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

#ifdef CURSES
#include <CUnit/CUCurses.h>
#else
#include <CUnit/Console.h>
#endif

void recursiveTest(Stack* S, int depth) {
    int val[2], *out;

    if (depth < 1000) {
	val[0] = rand();
	val[1] = rand();
	CU_ASSERT(S->length() == depth);
	CU_ASSERT(S->push(val[0], val[1]) == 1);

	//CU_ASSERT(peek(S, &h1, &h2) == 1);
	//CU_ASSERT(h1 == x);
	//CU_ASSERT(h2 == y);

	recursiveTest(S, depth+1);

	CU_ASSERT(S->length() == depth + 1);

	CU_ASSERT(S->pop(&out) == 1);

	CU_ASSERT(S->length() == depth);
	CU_ASSERT(out[0] == val[0]);
	CU_ASSERT(out[1] == val[1]);
    }
}


void testPile() {
    Stack* S = new Stack(2);
    recursiveTest(S, 0);
    delete S;
}

/*void recursiveTestTodo(Todo* S, int depth) {
    int val[2], *out;

    if (depth < 1000) {
	val[0] = rand();
	val[1] = rand();
	CU_ASSERT(S->length() == depth);
	CU_ASSERT(S->push(val[0], val[1]) == 1);

	//CU_ASSERT(peek(S, &h1, &h2) == 1);
	//CU_ASSERT(h1 == x);
	//CU_ASSERT(h2 == y);

	recursiveTest(S, depth+1);

	CU_ASSERT(S->length() == depth + 1);

	CU_ASSERT(S->pop(&out) == 1);

	CU_ASSERT(S->length() == depth);
	CU_ASSERT(out[0] == val[0]);
	CU_ASSERT(out[1] == val[1]);
    }
}


void testPile() {
    Stack* S = new Stack(2);
    recursiveTest(S, 0);
    delete S;
}*/

int main() {
    srand(time(NULL));
    if (CU_initialize_registry() == CUE_NOMEMORY) {
	fprintf(stderr, "Memory allocation failed\n");
	exit(1);
    }
    
    CU_pSuite def = CU_add_suite("Piles", NULL, NULL);

    CU_add_test(def, "Maniement de piles", testPile);

#ifdef CURSES
    CU_curses_run_tests();
#else
    CU_console_run_tests();
#endif

    CU_cleanup_registry();
    return 0;
}
