#include "../src/level.hpp"
#include "../src/ui/graphicsCurses.hpp"
#include "../src/network/server.hpp"

int main(int argc, char** argv) {
    int x, y, player, type;
    Level* game = new Level(40);
    Graphics* UI = new Graphics(game);
    Server* S;

    if (argc == 2)
	S = new Server(argv[1], 5);
    else
	S = new Server();

    S->acceptConnection();

    UI->initMap();
    UI->refreshMap();
    while (UI->getAction(&x, &y, &type) != 'q') {
	game->place(x, y, 1, type);
	UI->refreshMap();
	S->okuru(x, y, 1, type);
	S->morai();
	S->getMove(&x, &y, &player, &type);
	game->place(x, y, player, type);
	UI->refreshMap();
    }

    S->closeConnection();

    return 0;
}
