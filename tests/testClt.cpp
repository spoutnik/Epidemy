#include "../src/network/client.hpp"

int main(int argc, char** argv) {
    char input[100];
    int status = 1;
    if (argc < 3)
	exit(1);

    Client* C = new Client(argv[1], argv[2]);
    C->connectSrv();

    while(status != Network::packetTypes::QUIT) {

	printf("epdm> ");
	scanf("%s", input);

	if (strncmp(input, "HELLO", 5*sizeof(char)) == 0) {
	    printf("Sending hey !\n");
	    C->okuru(Network::packetTypes::GDAY);
	} else if (strncmp(input, "QUIT", 4*sizeof(char)) == 0) {
	    printf("Sending goodbye.\n");
	    status = Network::packetTypes::QUIT;
	    C->okuru(Network::packetTypes::QUIT);
	    continue;
	} else {
	    C->okuru(input);
	}

	C->morai();
    }

    return 0;
}
