#include "../src/network/server.hpp"

int main(int argc, char** argv) {
    char buffer[100];
    int status = 0;
    Server* S;

    if (argc == 2)
	S = new Server(argv[1], 5);
    else
	S = new Server();

    S->acceptConnection();

    status = S->morai();

    while(status != Network::packetTypes::QUIT) {
	if (status == Network::MESG) {
	    S->getMessage(buffer, 100);
	    printf("%s\n", buffer);
	}

	S->okuru(Network::packetTypes::RCVD);

	status = S->morai();
    }

    S->closeConnection();

    return 0;
}
