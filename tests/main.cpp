#include "../src/player.hpp"
#include "../src/ui/console.hpp"

int main(void) {
    printf("Epidemy Clone - v0.0\n");

    int turn = 1, done = 0;
    int player, x, y, type;

    Level* L = new Level(40);

    while (!done) {
	player = turn%2 + 1;

	printf("Turn %d - Player %d: \n", turn, player);
	scanf("%d %d %d", &x, &y, &type);
	printf("input: %d %d %d\n", x, y, type);
	
	if (type == 0)
	    done = 1;

	if (L->place(x, y, player, type))
	    turn++;
	render_map(L);
    }

    delete L;

    return 0;
}

