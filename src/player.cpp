#include "player.hpp"

int Player::count = 0;

Player::Player() {
    this->id = (Player::count += 1);

    this->nb_c2 = 0;
    this->nb_c3 = 0;
    this->nb_virus = 0;

    this->timer_c2 = 0;
    this->timer_c3 = 0;
    this->timer_virus = 0;
}

Player::~Player() {
    Player::count -= 1;
}

void Player::echoid() {
    printf("This is player %d\n", this->id);
}
