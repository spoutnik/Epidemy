/*! \file
 * \brief A generic stack to store ints
 *
 * Contains the Stack class definition */

#ifndef EPIDEMY_STACK_HPP
#define EPIDEMY_STACK_HPP

#include <cstdio>
#include <stdlib.h>

/*   _             _    
 ___| |_ __ _  ___| | __
/ __| __/ _` |/ __| |/ /
\__ \ || (_| | (__|   < 
|___/\__\__,_|\___|_|\_\ */

/*! \class Stack
 * \brief A chained list designed to store a variable number of ints */

class Stack {
    public:
	/*! \brief Default constructor
	 * 
	 * \param fieldNumber The number of fields each element of the stack must possess */
	Stack(int fieldNumber);

	/*! \brief Default destructor */
	~Stack();

	/*! \brief Remove the first element of the stack
	 *
	 * \param valueList A pointer where to store the content of the popped element 
	 * 
	 * \return `1` if the element could be popped, `0` if not
	 *
	 * \warning This does not free the memory taken by the element's content ! */
	int pop(int** valueList);

	/*! \brief Add an element to the stack
	 *
	 * \param firstInt The `fieldNumber` `int`s to store in the new element */
	int push(int firstInt, ... );
	
	//TODO
	/*! \brief Check if an element of the stack matches the arguments
	 *
	 * \param firstInt The list of int to look for
	 *
	 * \return `1` if found, `0` if not */
	int check(int firstInt, ... );

	/*! \brief Number of fields of the stack's elements
	 *
	 * \return The number of fields of the stack's elements */
	int getFieldNumber();

	/*! \brief Length of the stack
	 *
	 * \return The length of the stack */
	int length();

	/*! \brief Print the stack */
	void print(FILE* stream);

	void print() {
	    this->print(stdout);
	}

    private:
	/*! \brief A chained list structure */ 
	struct el {
	    /// A list of `int`
	    int* valueList;

	    /// The next element in the list: `NULL` indicates the end of the list
	    struct el* next;
	};

	/// An easier way to type `struct el *`
	typedef struct el* chain;

	/*! \brief The number of `integers` each element contains
	 *
	 * This equals the length of `valueList` in each `struct el` */
	int fieldNumber;

	/*! \brief The data structure storing all the actual information 
	 *
	 * The last inserted element is the first one in this chain */
	chain container;
};

#endif
