/*! \file
 * \brief stack implementation
 *
 * Contains the Stack class implementation */

#include "stdarg.h"
#include "stack.hpp"

Stack::Stack(int fieldNumber) {
    this->fieldNumber = fieldNumber;
    this->container = NULL;
}

Stack::~Stack() {
    int *values;
    while (this->pop(&values))
	free(values);
}

int Stack::pop(int** valueList) {
    if (!this->container)
	return 0;

    *valueList = this->container->valueList;

    chain tmp = this->container;
    this->container = this->container->next;
    free(tmp);

    return 1;
}

int Stack::getFieldNumber() {
    return this->fieldNumber;
}

/*int Stack::check(int x, int y) {
    chain content = this->container;

    while (content != NULL) {
	if (content->x == x && content->y == y) return 1;
	content = content->next;
    }
    return 0;
}*/

int Stack::push(int firstEl, ... ) {
    chain new_el;
    va_list args;

    if ((new_el = (chain) malloc(sizeof(struct el))) == NULL) {
	fprintf(stderr, "Error allocating memory\n");
	return 0;
    }
    
    new_el->valueList = (int*) malloc(sizeof(int)*this->fieldNumber);

    va_start(args, firstEl);

    new_el->valueList[0] = firstEl;
    for (int i = 1; i < this->fieldNumber; i++)
	new_el->valueList[i] = va_arg(args, int);

    va_end(args);

    new_el->next = this->container;
    this->container = new_el;
    return 1;
}

int Stack::length() {
    int depth = 0;
    chain current = this->container;

    while(current != NULL){
	depth++;
	current = current->next;
    }

    return depth;
}

void Stack::print(FILE* stream) {
    chain current = this->container;

    while (current) {
	for (int i = 0; i < this->fieldNumber; i++)
	    fprintf(stream, "%d ", current->valueList[i]);
	fprintf(stream, "\n");
	current = current->next;
    }
}
