#ifndef EPIDEMY_TODO_HPP
#define EPIDEMY_TODO_HPP

#include "stack.hpp"

/*____     _ _ ____  _             _    
 / ___|___| | / ___|| |_ __ _  ___| | __
| |   / _ \ | \___ \| __/ _` |/ __| |/ /
| |__|  __/ | |___) | || (_| | (__|   < 
 \____\___|_|_|____/ \__\__,_|\___|_|\_\ */

/*! \class CellStack
 *
 * \brief A stack dedicated to cell placement */

class CellStack: public Stack {
    public:
	/*! \brief Default constructor
	 *
	 * This specialization of `Stack` bears only 4 fields for you to use:
	 * - `x`, `y`: the coordinates of the cell
	 * - `player`
	 * - `type` */
	CellStack();
	//~Todo();

	/*! \brief Add an element to the todo pile
	 *
	 * \param x,y,player,type The data to put in the new element  
	 *
	 * \returns `1`, `0` if an error occured */
	int push(int x, int y, int player, int type);

	/*! \brief Remove an element from the todo pile
	 *
	 * \param x,y,player,type Pointers where to store the data  
	 *
	 * \returns `1` if the element could be popped, `0` if the pile is empty */
	int pop(int* x, int* y, int* player, int* type);
};

#endif
