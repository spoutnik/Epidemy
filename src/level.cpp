/*! \file
 * \brief level implementation */

#include "level.hpp"

#define VERBOSE 0

Level::Level(int size) {
    this->size = size;

    this->todo = new CellStack;
    this->todoLater = new CellStack;
    this->moves = new CellStack;

    this->grid = (Cell***) malloc(size*sizeof(Cell**));
    for (int i = 0; i < size; ++i) {
	this->grid[i] = (Cell**) malloc(size*sizeof(Cell*));
	for (int j = 0; j < size; ++j) {
	    grid[i][j] = new Cell();
	}
    }
}

Level::~Level() {
    for (int i = 0; i < this->size; ++i) {
	for (int j = 0; j < this->size; ++j) {
	    delete grid[i][j];
	}
	free(this->grid[i]);
    }

    delete this->todo;
    delete this->todoLater;
    free(grid);
}

int Level::getSize() {
    return (this->size);
}

Cell* Level::getCell(int x, int y) {
    if (!(x >= 0 && x < this->size && y >= 0 && y < this->size))
	return NULL;
    return (this->grid[x][y]);
}

void Level::reset() { // Reset method
    for (int i = 0; i < this->size; i++) {
	for (int j = 0; j < this->size; j++) {
	    this->grid[i][j]->reset();
	}
    }
}

/* Check the move to see which type of propagation it will require
 * (all adjacent cells OR adjacent cells of same type only) */
int Level::getAction(int x, int y, char player, char type) {
    Cell* c = this->grid[x][y];

    if (type == Cell::VIRUS)
	return Level::VIRUS;

    if (c->getType() == type && type == Cell::LARGE && c->getPlayer() == player)
	return Level::HARDENING;

    return Level::PLACE;
}

/* Add every adjacent cell to the stack if the action is valid */
void Level::consider(int x, int y, char player, char type) {
    if (type == 0) return;

    if (x-1 >= 0) this->todoLater->push(x-1, y, player, type);
    if (x+1 < this->size) this->todoLater->push(x+1, y, player, type);
    if (y-1 >= 0) this->todoLater->push(x, y-1, player, type);
    if (y+1 < this->size) this->todoLater->push(x, y+1, player, type);
}

/* Empties the todo stack, upgrading each cell: generates next todo stack doing so */
int Level::update(char propagationType) {
    int x, y, nb_done = 0, player, type;

    //todo->print();

    while (this->todo->pop(&x, &y, &player, &type)) {		    // For every cell in the stack
	if (VERBOSE) printf("[%d, %d]: ", x, y);
	switch (this->grid[x][y]->upgrade(player, type, propagationType)){  // Upgrade it and get return code
	    case 0:
		break;

	    case 1:
		consider(x, y, player, --type);   // if normal, type decreases
		nb_done++;
		break;

	    case 2:
		consider(x, y, player, type);	    // if an upgrade occured, type is the same
		nb_done++;
		break;
	}
    }

    // switch stacks
    CellStack* tmp = this->todo;
    this->todo = this->todoLater;
    this->todoLater = tmp;

    return (nb_done != 0);
}

int Level::place(int x, int y, char player, char type) {
    int propagationType = this->getAction(x, y, player, type);
    type = (propagationType == VIRUS ? this->grid[x][y]->getType() : type);

    this->todo->push(x, y, player, type);

    if (this->update(propagationType)) {
	this->moves->push(x, y, player, type);
	if (propagationType == Level::PLACE) propagationType = Level::WATERFALL;
	while (this->update(propagationType));
    } else {
	return 0;
    }

    this->reset();

    return propagationType;
}

/*void Level::undo() {
    for (int i = 0; i < this->getSize(); i++) {
	for (int j = 0; j < this->getSize(); j++) {
	    this->grid[

}*/
