#include "console.hpp"

void renderCell(Cell* c) {
    switch (c->getPlayer()) {
	case 1:
	    printf(KBLU);
	    printf(KBLD);
	    break;
	case 2:
	    printf(KRED);
	    printf(KBLD);
	    break;
	case 3:
	    printf(KGRN);
	    printf(KBLD);
	    break;
	case 4:
	    printf(KYEL);
	    printf(KBLD);
	    break;
    }

    switch (c->getType()) {
	case Cell::BLANK:
	    printf("_");
	    break;
	case Cell::WALL:
	    printf("█");
	    break;
	case Cell::SMALL:
	    printf(".");
	    break;
	case Cell::MEDIUM:
	    printf("o");
	    break;
	case Cell::LARGE:
	    printf("0");
	    break;
    }
    
    printf(KWHT);
    printf(KNBL);
    printf(" ");
}

void render_map(Level* map) {
    for (int i = 0; i < map->getSize(); i++) {
	for (int j = 0; j < map->getSize(); j++) {
	    renderCell(map->getCell(i, j));
	}
	printf("\n");
    }
}

