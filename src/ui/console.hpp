#ifndef DISPLAY_HPP
#define DISPLAY_HPP

#include "../level.hpp"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define KBLD  "\e[1m"
#define KNBL  "\e[0m"

void render_map(Level* map);

#endif
