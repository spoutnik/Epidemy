/*! \file
 * \brief graphics header */

#ifndef EPIDEMY_UI_HPP
#define EPIDEMY_UI_HPP

#include <ncurses.h>
#include "../level.hpp"

/*                     _     _          
  __ _ _ __ __ _ _ __ | |__ (_) ___ ___ 
 / _` | '__/ _` | '_ \| '_ \| |/ __/ __|
| (_| | | | (_| | |_) | | | | | (__\__ \
 \__, |_|  \__,_| .__/|_| |_|_|\___|___/
 |___/          |_| */

/*! \class Graphics
 * \brief A Ncurses interface */
class Graphics {
    public:

	/// Initializes `ncurses`
	Graphics(Level* game);

	/// Ends the `ncurses` session
	~Graphics();

	/*! \brief Create the game window
	 *
	 * \returns `1` if the window could be initialized correctly, `0` if not.
	 *
	 * If the terminal window is too small, `initMap` will fail. */
	int initMap();

	/*! \brief Refresh the game window */
	void refreshMap();

	/*! \brief Interpret user input 
	 * 
	 * \return A status code
	 *
	 * Listens to user input in an infinite loop, exits when `q` is pressed. */
 	int interpret();

	int getAction(int* x, int* y, int* type);

    private:

	/// The Level associated with the session
	Level* game;

	/// The window in which the map is drawn
	WINDOW* map;

	/*! \brief Display a character associated with `type`
	 * \param type From `Cell::types` */
	void typeTranslator(int type);

	/*! \brief Display the cell on teh screen
	 * \param C The cell to display */
	void putCell(Cell* C);

	/*! \brief The height of the grid */
	int mapy;

	/*! \brief The width of the grid */
	int mapx;

	/*! \brief The latitude of the cursor */
	int cursory;

	/*! \brief The longitude of the cursor */
	int cursorx;

};

#endif
