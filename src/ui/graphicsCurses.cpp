/*! \file
 * \brief graphics implementation */

#include "graphicsCurses.hpp"

#define screenX (mapx + 2*cursorx)
#define screenY (mapy + cursory)

int player = 1;

Graphics::Graphics(Level* game) {
    initscr();
    raw();
    keypad(stdscr, TRUE);
    noecho();
    start_color();

    refresh();

    this->game = game;

    init_pair(1, COLOR_BLUE, COLOR_BLACK);
    init_pair(2, COLOR_RED, COLOR_BLACK);
    init_pair(3, COLOR_YELLOW, COLOR_BLACK);
    init_pair(4, COLOR_GREEN, COLOR_BLACK);
}

Graphics::~Graphics() {
    endwin();
}

WINDOW* createWindow(int height, int width, int y, int x) {
    WINDOW* newWin;

    newWin = newwin(height, width, y, x);
    box(newWin, 0, 0);

    wrefresh(newWin);
    return newWin;
}

int Graphics::initMap() {
    if (this->game->getSize() > COLS || this->game->getSize() > LINES)
	return 0;

    this->map = createWindow(LINES, COLS, 0, 0);
    this->mapx = (COLS - this->game->getSize()*2)/2;
    this->mapy = (LINES - this->game->getSize())/2;
    cursorx = (cursory = 0);
    
    return 1;
}

void Graphics::typeTranslator(int type) {
    switch (type) {

	case Cell::BLANK:
            wprintw(this->map, "_ ");
            break;

	case Cell::WALL:
            wprintw(this->map, "X ");
            break;

	case Cell::SMALL:
            wprintw(this->map, ". ");
            break;

	case Cell::MEDIUM:
            wprintw(this->map, "o ");
            break;

	case Cell::LARGE:
            wprintw(this->map, "0 ");
            break;
    }
}

void Graphics::putCell(Cell* C) {
    wattron(this->map, COLOR_PAIR(C->getPlayer()));
    wattron(this->map, A_BOLD);
    typeTranslator(C->getType());
    wattroff(this->map, A_BOLD);
    wattroff(this->map, COLOR_PAIR(C->getPlayer()));
}

void Graphics::refreshMap() {
    mvwprintw(this->map, 0, 0, "Player: %d", player);
    for (int i = 0; i < this->game->getSize(); i++) {
	wmove(this->map, mapy+i, mapx);
	for (int j = 0; j < this->game->getSize(); j++) {
	    wmove(this->map, mapy+i, mapx+2*j);
	    this->putCell(this->game->getCell(i, j));
	}
    }
    wmove(this->map, screenY, screenX);
    wrefresh(this->map);
}

int Graphics::interpret() {
    int c;
    while ((c = getch()) != 'q') {
	switch(c) {

	    case KEY_UP:
		--cursory;
		break;

	    case KEY_DOWN:
		++cursory;
		break;

	    case KEY_LEFT:
		--cursorx;
		break;

	    case KEY_RIGHT:
		++cursorx;
		break;

	    case '1':
		this->game->place(cursory, cursorx, player, Cell::SMALL);
		break;

	    case '2':
		this->game->place(cursory, cursorx, player, Cell::MEDIUM);
		break;

	    case '3':
		this->game->place(cursory, cursorx, player, Cell::LARGE);
		break;

	    case 'v':
		this->game->place(cursory, cursorx, player, Cell::VIRUS);
		break;

	    case 'p':
		player = 1 + (player%4);
		break;
	}

	wmove(this->map, screenY, screenX);
	this->refreshMap();
    }

    return c;
}

int Graphics::getAction(int* x, int* y, int* type) {
    int c;
    while ((c = getch()) != 'q') {
	switch(c) {

	    case KEY_UP:
		--cursory;
		break;

	    case KEY_DOWN:
		++cursory;
		break;

	    case KEY_LEFT:
		--cursorx;
		break;

	    case KEY_RIGHT:
		++cursorx;
		break;

	    default:
		break;

	    case '1':
		*x = cursory;
		*y = cursorx;
		*type = Cell::SMALL;
		return Cell::SMALL;

	    case '2':
		*x = cursory;
		*y = cursorx;
		*type = Cell::MEDIUM;
		return Cell::MEDIUM;

	    case '3':
		*x = cursory;
		*y = cursorx;
		*type = Cell::LARGE;
		return Cell::LARGE;

	    case 'v':
		*x = cursory;
		*y = cursorx;
		*type = Cell::VIRUS;
		return Cell::VIRUS;
	}

	wmove(this->map, screenY, screenX);
	this->refreshMap();
    }

    return c;
}

void title(WINDOW* win, char* title) {
    mvwprintw(win, 0, 1, title);
    wrefresh(win);
}

void eraseWin(WINDOW* win) {
    wborder(win, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
    wrefresh(win);
    delwin(win);
}

void logWin(WINDOW* win, char* msg) {
    int x, y;
    getmaxyx(win, y, x);
    x++;

    mvwprintw(win, y - 2, 2, msg);
    wrefresh(win);
}

