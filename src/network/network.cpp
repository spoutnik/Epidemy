/*! \file
 * \brief network implementation
 *
 * Contains the Network class implementation */

#include "network.hpp"

#define VERBOSE 0

int Network::lookUp(const char* host, const char* port) {
    struct addrinfo hints;
    memset(&hints, '\0', sizeof(struct addrinfo));

    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = (host == NULL ? AI_PASSIVE : 0);

    return getaddrinfo(host, port, &hints, &this->results);
}

int Network::createSock() {
    if ((this->sockfd = socket(this->results->ai_family, this->results->ai_socktype, this->results->ai_protocol)) == -1)
	fprintf(stderr, "Error creating socket: %s\n", strerror(errno));
    if (VERBOSE) printf("Socket File Descriptor: %d\n", this->sockfd);

    return this->sockfd;
}

int Network::readPacket(int sockfd) {
    int readBytes;
    char length;
    memset(this->packet, '\0', sizeof(char)*100);

    if (read(sockfd, &length, sizeof(char)) == -1) {
	fprintf(stderr, "Error receiving size: %s\n", strerror(errno));
    }

    if ((readBytes = recv(sockfd, this->packet, (length+1)*sizeof(char), 0)) == -1) {
	fprintf(stderr, "Error receiving packet: %s\n", strerror(errno));
    }

    return length;
}

int Network::sendPacket(int sockfd) {
    int status;

    if ((status = send(sockfd, this->packet, (packet[0] + 1)*sizeof(char), 0)) == -1) {
	fprintf(stderr, "Error sending packet: %s\n", strerror(errno));
    }

    return status;
}

int Network::unpack() {
    int category = this->packet[0];
    return category;
}

void Network::getMove(int* x, int* y, int* player, int* type) {
    *x = this->packet[1];
    *y = this->packet[2];
    *player = this->packet[3];
    *type = this->packet[4];
}

void Network::pack(char type) {
    memset(this->packet, '\0', 100*sizeof(char));

    this->packet[1] = type;
    this->packet[0] = 1;
}

void Network::pack(const char* message) {
    memset(this->packet, '\0', 100*sizeof(char));

    this->packet[1] = MESG;
    strncat(this->packet+2, message, strlen(message)*sizeof(char));
    this->packet[0] = (char) (strlen(this->packet + 1) + 1);
}

void Network::pack(int x, int y, int player, int type) {
    memset(this->packet, '\0', 100*sizeof(char));

    this->packet[1] = MOVE;
    this->packet[2] = x;
    this->packet[3] = y;
    this->packet[4] = player;
    this->packet[5] = type;
    this->packet[0] = 5;
}

int Network::getMessage(char* buffer, int size) {
    int is_message = (this->packet[0] == MESG);

    if (is_message && buffer && size > 0)
	memcpy(buffer, this->packet, size);

    return is_message;
}

int Network::getType() {
    return this->packet[0];
}
