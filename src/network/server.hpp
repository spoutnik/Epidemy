/*! \file
 * \brief server header */

#include "network.hpp"

#define DEFAULT_BACKLOG 5

/*___                           
/ ___|  ___ _ ____   _____ _ __ 
\___ \ / _ \ '__\ \ / / _ \ '__|
 ___) |  __/ |   \ V /  __/ |   
|____/ \___|_|    \_/ \___|_| */

/*! \class Server
 * \brief %Server counterpart of the network implementation */
class Server: public Network {
    public:
	/*! \brief Constructor 
	 *
	 * \param portNo The port to listen on
	 * \param backLog The size of the waiting queue
	 *
	 * Uses `Network::lookUp` and `Network::createSock`*/
	Server(const char* portNo, int backLog);

	/*! \brief Constructor 
	 *
	 * Uses default values to call `Server::Server`. `Network::lookUp` and `Network::createSock` are used.*/
	Server() : Server(DEFAULT_PORT, DEFAULT_BACKLOG) {}

	/*! \brief Destructor
	 *
	 * Closes the socket once done. */
	~Server();

	/*! \brief Accept incoming connection
	 *
	 * \return The file descriptor associated with the new client 
	 *
	 * Sets `Server::clientfd` */
	int acceptConnection();

	/*! \brief Closes the client's connection */
	void closeConnection();
	
    private:
	const char* portNo;
	int backLog;
	int clientfd;

	struct sockaddr_storage clientAddresses;
	socklen_t length;

	int setup();

	int sendPacket();
	int readPacket();
};
