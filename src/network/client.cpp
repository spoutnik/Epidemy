/*! \file
 * \brief client implementation
 *
 * Contains the Client class implementation */

#include "client.hpp"

Client::Client(const char* host, const char* portNo) {
    this->host = host;
    this->portNo = portNo;

    this->lookUp(host, portNo);
    this->createSock();
}

Client::~Client() {
    close(this->sockfd);
    freeaddrinfo(this->results);
}


int Client::connectSrv() {
    int status;
    if ((status = connect(this->sockfd, this->results->ai_addr, this->results->ai_addrlen)) == -1)
	fprintf(stderr, "Error connecting: %s\n", strerror(errno));
    return status;
}

int Client::readPacket() {
    return Network::readPacket(this->sockfd);
}

int Client::sendPacket() {
    return Network::sendPacket(this->sockfd);
}
