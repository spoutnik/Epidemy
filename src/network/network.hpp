/*! \file
 * \brief network header
 *
 * Contains the Network class definition */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>

#define DEFAULT_PORT "10001"

/*   _      _                      _    
| \ | | ___| |___      _____  _ __| | __
|  \| |/ _ \ __\ \ /\ / / _ \| '__| |/ /
| |\  |  __/ |_ \ V  V / (_) | |  |   < 
|_| \_|\___|\__| \_/\_/ \___/|_|  |_|\_\ */

/*! \class Network 
 * \brief A base class for network entities 
 *
 * This class contains the shared properties of clients and servers, 
 * such as the address lookup, and packet handling. */

class Network {
    public:
	/*! \enum packetTypes
	 * \brief The recognised types of packets */
	enum packetTypes {
	    GDAY = 1,	    //!< Opening the conversation
	    RCVD,	    //!< Message received
	    MESG,	    //!< The packet contains a message
	    MOVE,	    //!< The packet contains a move (4 ints)
	    QUIT = 0,	    //!< Close conversation
	    UNKW	    //!< Unknown packet
	};
	
	/*! \brief Receive a message
	 *
	 * Will store the received message in `Network::packet`.
	 *
	 * \return The type of packet, as decribed in `Network::packetTypes` */
	int morai() {
            readPacket();

            return unpack();
        }

	/*! \brief Send a packet
	 *
	 * \param type The type of packet to send, as described in `Network::packetTypes`
	 *
	 * Will store the generated packet in `Network::packet`. */
        void okuru(char type) {
            pack(type);

            sendPacket();
        }

	/*! \brief Send a message
	 *
	 * \param message The string to store in the packet
	 *
	 * The packet will be of type `Network::packetTypes::MESG`. Will store the generated packet in `Network::packet`. */
        void okuru(const char* message) {
            pack(message);

            sendPacket();
        }

	/*! \brief Send a move
	 *
	 * \param x,y,player,type The move's data
	 *
	 * The packet will be of type `Network::packetTypes::MOVE`. Will store the generated packet in `Network::packet`. */
        void okuru(int x, int y, int player, int type) {
            pack(x, y, player, type);

            sendPacket();
        }

	void getMove(int* x, int* y, int* player, int* type);

	/*! \brief Method to get the last received message
	 *
	 * \param buffer A pointer towards a char buffer
	 * \param size The buffer's size
	 *
	 * Copies the content of the last message into the target buffer. If it is NULL or its size equals zero, it does nothing.
	 *
	 * \return `1` if the packet contains a message, `0` if not. */
	int getMessage(char* buffer, int size);

	/*! \brief Method to get the type of the last received message
	 *
	 * \return An int as specified in `Network::packetTypes` */
	int getType();

    protected:
	/*! \brief A placeholder for the socket's file descriptor */
	int sockfd;

	/*! \brief Storage for `Network::lookUp` results */
	struct addrinfo* results;

	/*! \brief The packet */
	char packet[100];

	/*! \brief Perform a `getaddrinfo` to get a TCP connection to the desired host.
	 *
	 * \param host The host to look for
	 * \param port The port to connect to
	 *
	 * Populates `Network::results` with information to connect to the target.
	 *
	 * \return The return value of `getaddrinfo` */
	int lookUp(const char* host, const char* port);

	/*! \brief Create a socket
	 *
	 * Sets `Network::sockfd` if the socket could be created. Uses `Network::results`,
	 * so must be called after `Network::lookUp`.
	 * \return `Network::sockfd` */
	int createSock();

	/*! \brief The default method to read a packet
	 *
	 * Must be implemented by the object inheriting network */
	virtual int readPacket() = 0;

	/*! \brief Read a packet from `sockfd`
	 *
	 * \warning The packet must follow protocol !
	 * This function will block the program until a packet is read.
	 *
	 * \param sockfd The file descriptor where to read the packet 
	 *
	 * The packet will be stored in `Network::packet` */
	int readPacket(int sockfd);

	/*! \brief The default method to send a packet
	 *
	 * Must be implemented by the object inheriting network */
	virtual int sendPacket() = 0;

	/*! \brief Send a packet on `sockfd`
	 *
	 * \param sockfd The file descriptor to use */
	int sendPacket(int sockfd);

    private:
	int unpack();

	void pack(char type);
	void pack(const char* message);
	void pack(int x, int y, int player, int type);
};
