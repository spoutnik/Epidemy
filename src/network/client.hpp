/*! \file
 * \brief client header
 *
 * Contains the Client class definition */

#include "network.hpp"

/*____ _ _            _   
 / ___| (_) ___ _ __ | |_ 
| |   | | |/ _ \ '_ \| __|
| |___| | |  __/ | | | |_ 
 \____|_|_|\___|_| |_|\__| */

/*! \class Client
 * \brief %Client counterpart of the network capabilities */
class Client: public Network {
    public:
	/*! \brief Contructor
	 *
	 * \param host The host to connect to 
	 * \param portNo The port to connect to
	 *
	 * Performs `Network::lookUp` and `Network::createSock` */
	Client(const char* host, const char* portNo);

	/*! \brief Destructeur */
	~Client();

	/* \brief connect to a server
	 *
	 * \return Boolean set to `1` if the connection was successful */
	int connectSrv();

    private:
	/*! \brief The host */
	const char* host;

	/*! \brief The host */
	const char* portNo;

	/*! \brief Send a packet
	 *
	 * Sends a packet to the server
	 * 
	 * \return Size in bytes of packet sent */
	int sendPacket();

	/*! \brief Read a packet
	 *
	 * Receive a packet from the server
	 * 
	 * \return Size in bytes of packet received */
	int readPacket();
};
