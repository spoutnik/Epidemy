/*! \file
 * \brief server implementation */

#include "server.hpp"

Server::Server(const char* portNo, int backLog) {
    this->portNo = portNo;
    this->backLog = backLog;

    this->clientfd = 0;
    
    this->lookUp(NULL, portNo);
    this->createSock();
    this->setup();
}

Server::~Server() {
    close(this->clientfd);
    close(this->sockfd);
    freeaddrinfo(this->results);
}

void losepesky(int sockfd) {
    int yes = 1;
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof yes) == -1) {
	perror("setsockopt");
	exit(1);
    }
}

int Server::setup() {
    int status;

    losepesky(this->sockfd);

    if ((status = bind(this->sockfd, this->results->ai_addr, this->results->ai_addrlen)) == -1) {
	fprintf(stderr, "Error binding socket: %s\n", strerror(errno));
	return status;
    }

    if ((status = listen(this->sockfd, this->backLog)) == -1) {
	fprintf(stderr, "Error listening: %s\n", strerror(errno));
	return status;
    }

    printf("Listening on socket %d, port: %s\n", this->sockfd, this->portNo);
    return status;
}

int Server::acceptConnection() {
    if ((this->clientfd = accept(this->sockfd, (struct sockaddr*)&clientAddresses, &this->length)) == -1)
	fprintf(stderr, "Error accepting: %s\n", strerror(errno));

    return this->clientfd;
}

void Server::closeConnection() {
    close(this->clientfd);
}

int Server::readPacket() {
    return Network::readPacket(this->clientfd);
}

int Server::sendPacket() {
    return Network::sendPacket(this->clientfd);
}
