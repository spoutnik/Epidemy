/*! \file
 * \brief cell header */

#ifndef EPIDEMY_CELL_HPP
#define EPIDEMY_CELL_HPP

#include<stdio.h>

/*        _ _ 
  ___ ___| | |
 / __/ _ \ | |
| (_|  __/ | |
 \___\___|_|_| */

/*! \class Cell
 *
 * \brief Objects making up the grid */

class Cell {

    public:
	/// Recognized types of `Cell`
	enum types {
	    BLANK = 0,	    //!< An empty `Cell`
	    SMALL,	    //!< A small `Cell`	
	    MEDIUM,	    //!< A medium `Cell`
	    LARGE,	    //!< A large `Cell` 
	    WALL,	    //!< A hardened `Cell`
	    VIRUS	    //!< A virus: can be used, but never 'placed'
	};

	/*! \brief Default constructor
	 *
	 * Builds a `BLANK` `Cell` with empty player field */
	Cell();

	/*! \brief Alternate constructor
	 *
	 * \param type A type of `Cell` (`NORMAL`, `MEDIUM`, `LARGE`, `WALL`)
	 * \param player A Player's number, `0` for none.
	 *
	 * Builds a cell of type `type` owned by player `player` */
	Cell(char player, char type);

	/*! \brief Get the `Cell`'s type
	 *
	 * \return The `Cell`'s type. */
	char getType();

	/*! \brief Get the `Cell`'s player
	 *
	 * \return The `Cell`'s player. */
	char getPlayer();

	/*! \brief Upgrade a `Cell`
	 *
	 * \param player The player trying to acquire the `Cell`
	 * \param type The type of `Cell` trying to take over
	 * \param propagationType A type of propagation, from `Level::propagationTypes`
	 *
	 * \return A status code:
	 *  - `0` if the `Cell` could not be upgraded;
	 *  - `1` if the `Cell` was updated as planned;
	 *  - `2` if the `Cell` updated and an event occured (level gain/Destruction)
	 *
	 * Send an update query to a `Cell`. If the update is granted, a return code is returned accordingly */
	int upgrade(char player, char type, char propagationType);
	
	/*! \brief Reset a `Cell`'s upgrade switch
	 *
	 * A `Cell`, once upgraded, will not be upgraded again, in order for the chain reaction not to be too intense. `reset` signals that the `Cell` is ready to be upgraded again */
	void reset();

	void kill() {
	    this->type = 0;
	    this->player = 0;
	}

    private:
	/// A boolean, set to `1` if the `Cell` has already been upgraded and should not upgrade again during the turn.
	char upgraded;
	
	/// The `Cell`'s type
	char type;

	/// The `Cell`'s allegiance
	char player;
};

#endif
