/*! \file
 * \brief level header
 *
 * Contains the Level class definition. */

#ifndef EPIDEMY_LEVEL_HPP
#define EPIDEMY_LEVEL_HPP

#include "cell.hpp"
#include "cellStack.hpp"

/*                _ 
| | _____   _____| |
| |/ _ \ \ / / _ \ |
| |  __/\ V /  __/ |
|_|\___| \_/ \___|_| */

/*! \class Level
 *
 * \brief The backbone of the game */

class Level {

    public:

	/// enum describing all propagation types that can occur
	enum propagationTypes {
	    /// The `Cell` has just been placed by the player: it can't take the opponent's `Cell`
	    PLACE, 
	    /// The `Cell` has been spawned by another `Cell` next to it: it can override the opponent's
	    WATERFALL, 
	    /// This propagation affects only adjacent `LARGE` `Cell`
	    HARDENING,
	    /// This propagation affects only adjacent `Cell` of same type
	    VIRUS
	};

	/*! \brief Default constructor
	 *
	 * \param size	    The size of the grid
	 *
	 * Initializes an empty level with a grid of size `size`. */
	Level(int size);

	/*! \brief Alternate constructor
	 *
	 * \param size	    The size of the grid
	 * \param layout    A `char` matrix with the layout of the level 
	 *
	 * Initializes a level with a grid of size `size`, and a layout as described in `layout`. */
	Level(int size, char** layout);

	/*! \brief Default destructor */
	~Level();

	/*! \brief Place a `Cell` of `player` and of type `type` on `(x; y)`
	 *
	 * \param x,y	    The coordinates of the target `Cell`
	 * \param player    The player placing the new `Cell`
	 * \param type	    The type of `Cell` being placed
	 *
	 * \return Boolean telling if the `Cell` has been placed correctly or not */
	int place(int x, int y, char player, char type);

	/*! \brief Get the grid's size
	 *
	 * \return The size of the grid. */
	int getSize();

	/*! \brief Get a specific `Cell`
	 *  
	 *  \param x,y	    The coordinates of the target `Cell`
	 *
	 *  \return A pointer towards that `Cell`, `NULL` if the coordiantes are invalid. */
	Cell* getCell(int x, int y);


	/*! \brief Dump the `Level::moves` history
	 *
	 * \param stream The stream where to dump it */
	void dumpMoves(FILE* stream) {
	    this->moves->print(stream);
	}
    
	void undo();
    private:
	/*! \brief Size of `Level::grid` */
	int size;

	/*! \brief `Cell` * matrix 
	 *
	 * Contains pointers toward all the `Cell`s that make up the board. */
	Cell*** grid;

	/*! \brief The current `todo`
	 *
	 * Contains the list of `Cell`s to upgrade during the current step of the Breadth-first search */
	CellStack* todo;
	
	/*! \brief The buffer `todo`
	 *
	 * Contains the list of `Cell`s to upgrade during the next step of the Breadth-first search */
	CellStack* todoLater;

	/*! \brief The list of moves made by the players during the game
	 *
	 * Contains every event that occured during the game. A level can be saved and restored by dumping and loading it. */
	CellStack* moves;

	/*! \brief Reset `Level::grid`
	 *
	 * Calls `Cell::reset` on every cell of the map.*/
	void reset();

	/*! \brief Update `Level::grid`, applying the changes resulting of the player's actions
	 *
	 * \param propagationType The type of propagation, as detected by `Level::getAction` 
	 *
	 * This function uses both `Level::todo` and `Level::todoLater`, emptying them in the process */
	int update(char propagationType);

	/*! \brief Determine the player's action
	 *
	 * \param x,y The coordinates where to act
	 * \param player The player's number
	 * \param type The type of `Cell` being placed (of `Cell::types`)
	 *
	 * \return The propagation type associated with the move (from `Level::propagationTypes`) */
	int getAction(int x, int y, char player, char type);

	/*! \brief Populate `Level::todoLater`
	 *
	 * \param x,y	    The coordinates of the target `Cell`
	 * \param player    The player placing the new `Cell`
	 * \param type	    The type of `Cell` being placed
	 *
	 * Adds all adjacent cells to `todoLater`, if they exist. */
	void consider(int x, int y, char player, char type);
};

#endif
