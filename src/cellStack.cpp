#include "cellStack.hpp"

CellStack::CellStack(): Stack(4) {
}

int CellStack::push(int x, int y, int player, int type) {
    return this->Stack::push(x, y, player, type);
}

int CellStack::pop(int* x, int* y, int* player, int* type) {
    int status, *values;
    status = this->Stack::pop(&values);

    if (status != 0) {
	*x = values[0];
	*y = values[1];
	*player = values[2];
	*type = values[3];
	free(values);
    }
    
    return status;
}
