/*! \file
 * \brief cell implementation */

#include "cell.hpp"
#include "level.hpp"

#define VERBOSE 0

Cell::Cell(char player, char type) {
    this->type = type;
    this->player = player;

    this->upgraded = 0;
}

Cell::Cell() {
    this->type = BLANK;
    this->player = 0;
    
    this->upgraded = 0;
}

void Cell::reset() {
    this->upgraded = 0;
}

char Cell::getType() {
    return this->type;
}

char Cell::getPlayer() {
    return this->player;
}

int Cell::upgrade(char player, char type, char propagationType) {
    if (VERBOSE) printf("Cell is type %d/%d, replacing by type %d/%d: ", this->type, this->player, type, player);
    int status;

    if (this->type > type || type == Cell::BLANK) return 0;	    // if new cell is inferior

    if (this->upgraded && type == this->type) {
	if (VERBOSE) printf("cell has already been upgraded\n");
	return 0;
    }

    switch (propagationType) {
	case Level::PLACE:
	    if (this->player == player) {
		if (this->type < type) {
		    this->type = type;

		    status = 1;
		} else if (this->type == type) {
		    this->type++;

		    status = 2;
		} else {
		    status = 0;
		}
	    } else {
		if (this->type < type) {
		    this->type = type;
		    this->player = player;

		    status = 1;
		} else {
		    status = 0;
		}
	    }
	    break;

	case Level::WATERFALL:
	    if (this->type < type) {
		this->type = type;
		this->player = player;

		status = 1;
	    } else if (this->type == type) {
		this->type++;
		this->player = player;

		status = 2;
	    } else {
		status = 0;
	    }
	    break;

	case Level::HARDENING:
	    if (this->type == Cell::LARGE) {
		this->type = WALL;
		this->player = player;

		status = 2;
	    } else {
		status = 0;
	    }
	    break;

	case Level::VIRUS:
	    if (type != Cell::WALL && this->type == type) {
		this->type = BLANK;
		this->player = 0;

		status = 2;
	    } else {
		status = 0;
	    }
	    break;
    }

    if (status) this->upgraded = 1;

    return status;
}
