#ifndef EPIDEMY_PLAYER_HPP
#define EPIDEMY_PLAYER_HPP

#include <stdio.h>

class Player {

    public:
	/// Default constructor
	Player();

	/// Default destructor
	~Player();

	/// Output the player's number
	void echoid();

	/// Get the percentage of acquired map
	int getCpc();

    private:
	/// A count to track the number of players
	static int count;

	/// The player's ID
	char id;

	/// Number of available cells
	char nb_c2, nb_c3, nb_virus;

	/// Time left to acquire a new cell
	char timer_c2, timer_c3, timer_virus;
	
	/// Cell PerCentage - percentage of acquired map
	float cpc;
};

#endif
