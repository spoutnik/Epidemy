SRC = src
BIN = bin
TST = tests
NET = bin/network
FLAGS = -std=c++11
LIBS = -lncurses
CC = g++

# Main programs

all: main

main: $(TST)/testUI.cpp $(BIN)/cell.o $(BIN)/level.o $(BIN)/player.o $(BIN)/stack.o $(BIN)/cellStack.o $(BIN)/ui/graphicsCurses.o
	$(CC) $(FLAGS) $(LIBS) $^ -o $(BIN)/$@ 

srv: $(TST)/testSrv.cpp $(NET)/network.o $(NET)/server.o
	$(CC) $(FLAGS) $^ -o $(BIN)/$@ 

clt: $(TST)/testClt.cpp $(NET)/network.o $(NET)/client.o
	$(CC) $(FLAGS) $^ -o $(BIN)/$@ 

# Tests

unitTests: $(TST)/unitTests.cpp $(BIN)/stack.o
	$(CC) $(FLAGS) $(LIBS) -lcunit $^ -o $(BIN)/$@ 

unitTestsCurses: $(TST)/unitTests.cpp $(BIN)/stack.o
	$(CC) $(FLAGS) $(LIBS) -lcunit -DCURSES $^ -o $(BIN)/unitTests

# Basic compilation

$(BIN)/%.o: $(SRC)/%.cpp
	$(CC) $(FLAGS) -c -o $@ $< $(CFLAGS)

$(BIN)/ui/%.o: $(SRC)/ui/%.cpp
	$(CC) $(FLAGS) -c -o $@ $< $(CFLAGS)

$(NET)/%.o: $(NET)/%.cpp
	$(CC) $(FLAGS) -c -o $@ $< $(CFLAGS)

# Meta

dox:
	#doxygen doc/config
	python ./doc/dox2html5.py ./doc/Doxyfile-mcss

# Clean up

clean:
	rm -rf bin/main bin/testUI bin/unitTests bin/unitTestsCurses doc/html/ doc/latex bin/srv bin/clt

purge: clean
	find bin -name "*.o" -exec rm '{}' \;
